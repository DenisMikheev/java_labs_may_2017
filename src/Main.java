import java.math.BigInteger;
import java.util.Random;

/**
 * Created by denis on 03.03.17.
 */
public class Main {
    public static void main(String[] args) {
        BigInteger b1 = new BigInteger(24, new Random());
        BigInteger b2 = new BigInteger(24, new Random());
        System.out.println("\n\n\n" + "Stolbik: " + Column.multiply(b1.toString(), b2.toString()));
        System.out.println("" + "Karatsuba: " + Karatsuba.multiply(b1, b2) + ", " + Karatsuba.time + " ns");
        System.out.println("BigInt: " + b1.multiply(b2));
        System.out.println(b1 + " X " + b2);

    }
}
