import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * Created by denis on 07.03.17.
 */
public class Column {
    public static String multiply(String A, String B)
    {
//        Date d1 = new Date();
        long st = System.nanoTime();
        String result;
        char[] Ac = A.toCharArray();
        char[] Bc = B.toCharArray();

        String Azn = "";
        String Bzn = "";
        String commonZn = "";
        int klim = 0;
        int ilim = 0;
        if (Objects.equals(String.valueOf(Ac[0]), "-"))
        {
            Azn = String.valueOf(Ac[0]);
            ilim = 1;
        }
        if (Objects.equals(String.valueOf(Bc[0]), "-"))
        {
            Bzn = String.valueOf(Bc[0]);
            klim = 1;
        }
        if (!Objects.equals(Azn, Bzn))
        {
            commonZn = "-";
        }


        int perenos;
        ArrayList<Integer> resultArray = new ArrayList<>();
        for (int i = 0; i < Ac.length + Bc.length; i++)
        {
            resultArray.add(0);
        }
//        System.out.println(resultArray);


        for (int i = Ac.length - 1; i >= ilim; --i)
        {
            perenos = 0;
            int answePos = 0;
            for (int k = Bc.length - 1; k >= klim; --k)
            {
                Integer proizv = Character.getNumericValue(Ac[i]) * Character.getNumericValue(Bc[k]);
//                System.out.println("Ac[i]=" + Character.getNumericValue(Ac[i]) + " Bc[k]=" + Character.getNumericValue(Bc[k]) + " = " + proizv);


                answePos = Ac.length + Bc.length + 1 + i + k - A.length() - B.length();

                proizv += resultArray.get(answePos);
                proizv += perenos;

//                System.out.println(proizv);
                resultArray.set(answePos, proizv%10);
                perenos = proizv/10;
//                System.out.println(resultArray);

            }
            resultArray.set(answePos-1, perenos);

        }


//        System.out.println(resultArray);

        result = "";
        for (Integer s: resultArray)
        {
            result += s;
        }
        result = commonZn + result.replaceAll("^0*", "");
//        Date d2 = new Date();
        long s2 = System.nanoTime();
        long diff = s2 - st;
        return result + ", " + diff + " ns";
    }
}
