import java.math.BigInteger;
import java.util.Date;

/**
 * Created by denis on 03.03.17.
 */
public class Karatsuba {
    public static long time = 0;

    public static BigInteger karatsuba(BigInteger x, BigInteger y)
    {
        int N = Math.max(x.bitLength(), y.bitLength());
        if (N <= 10) return x.multiply(y);
        N = (N/2) + (N%2);

        // x = a + 2^N b, y = c + 2^N d
        BigInteger b = x.shiftRight(N);
        BigInteger a = x.subtract(b.shiftLeft(N));
        BigInteger d = y.shiftRight(N);
        BigInteger c = y.subtract(d.shiftLeft(N));

        BigInteger ac = karatsuba(a, c);
        BigInteger bd = karatsuba(b, d);
        BigInteger abcd = karatsuba(a.add(b), c.add(d));
        BigInteger res = ac.add(abcd.subtract(ac).subtract(bd).shiftLeft(N)).add(bd.shiftLeft(2*N));

        return res;
    }

    public static BigInteger multiply(BigInteger x, BigInteger y)
    {
//        Date d1 = new Date();
        long d1 = System.nanoTime();
        BigInteger res = karatsuba(x ,y);
//        Date d2 = new Date();
        long d2 = System.nanoTime();
        time = d2 - d1;
        return res;
    }
}
